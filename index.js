const got = require('got');
const url = require('url');
const Path = require('path');

function generateAuthorizationHeader(applicationKeyId, applicationKey) {
    const credentialPair = Buffer.from(`${applicationKeyId}:${applicationKey}`, 'utf-8');
    return `Basic ${credentialPair.toString('base64')}`
}

const FSBackblaze = function (bucketName, applicationKeyId, applicationKey) {
    this.bucketName = bucketName;
    this.applicationKeyId = applicationKeyId;
    this.applicationKey = applicationKey;
    this.filedescriptors = {};
    this.statCache = {};

    this.accountId = null;
    this.authorizationToken = null;
    this.uri = null;
    this.downloadUrl = null;
};

FSBackblaze.prototype.authenticate = async function () {
    const self = this;
    if (!!self.accountId) {
        return;
    }

    const res = await got({
        url: 'https://api.backblazeb2.com/b2api/v2/b2_authorize_account',
        method: 'GET',
        headers: {
            'Authorization': generateAuthorizationHeader(self.applicationKeyId, self.applicationKey)
        }
    });

    const data = JSON.parse(res.body);
    self.bucketId = data['allowed']['bucketId'];
    self.accountId = data['accountId'];
    self.authorizationToken = data['authorizationToken'];
    self.uri = url.parse(Path.posix.join(data['apiUrl'], 'b2api', 'v2'));
    self.downloadUrl = url.parse(Path.posix.join(data['downloadUrl'], 'file', self.bucketName));
};

FSBackblaze.preparePath = function (path) {
    const pathParts = path.split(/[\\/]/);
    const filteredPathParts = [];

    for (let i = 0; i < pathParts.length; i++) {
        if (pathParts[i].length === 0) {
            continue;
        }

        filteredPathParts.push(encodeURIComponent(pathParts[i]));
    }

    return filteredPathParts.join('/');
}

FSBackblaze.prototype.access = async function (path, mode, callback) {
    await this.authenticate();

    if (typeof (callback) === 'undefined' && typeof (mode) === 'function') {
        callback = mode;
    }

    throw new Error('Method \'' + arguments.callee.name + '\' not Implemented');
};

FSBackblaze.prototype.appendFile = async function (file, data, options, callback) {
    await this.authenticate();

    throw new Error('Method \'' + arguments.callee.name + '\' not Implemented');
};

FSBackblaze.prototype.chmod = async function (path, mode, callback) {
    await this.authenticate();

    throw new Error('Method \'' + arguments.callee.name + '\' not Implemented');
};

FSBackblaze.prototype.chown = async function (path, uid, gid, callback) {
    await this.authenticate();

    throw new Error('Method \'' + arguments.callee.name + '\' not Implemented');
};

FSBackblaze.prototype.close = async function (fd, callback) {
    await this.authenticate();

    if (this.filedescriptors.hasOwnProperty(fd) === false) {
        callback('fd isn\'t a valid open file descriptor.');
        return;
    }

    delete this.filedescriptors[fd];
    callback(null);
};

FSBackblaze.prototype.createReadStream = async function (path, options) {
    await this.authenticate();

    options = options || {};
    options.start = options.start || 0;
    options.end = options.end || '';

    return got.stream({
        url: Path.posix.join(self.downloadUrl, path),
        prefixUrl: this.uri.href,
        headers: {
            'Range': 'bytes=' + options.start + '-' + options.end,
            'Authorization': this.authorizationToken
        }
    });
};

FSBackblaze.prototype.createWriteStream = async function (path) {
    await this.authenticate();

    throw new Error('Method \'' + arguments.callee.name + '\' not Implemented');
};

FSBackblaze.prototype.lchmod = async function (path, mode, callback) {
    await this.authenticate();

    throw new Error('Method \'' + arguments.callee.name + '\' not Implemented');
};

FSBackblaze.prototype.lchown = async function (path, uid, gid, callback) {
    await this.authenticate();

    throw new Error('Method \'' + arguments.callee.name + '\' not Implemented');
};

FSBackblaze.prototype.link = async function (existingPath, newPath, callback) {
    await this.authenticate();

    throw new Error('Method \'' + arguments.callee.name + '\' not Implemented');
};

FSBackblaze.prototype.lstat = async function (path, callback) {
    await this.authenticate();

    throw new Error('Method \'' + arguments.callee.name + '\' not Implemented');
};

FSBackblaze.prototype.mkdir = async function (path, options, callback) {
    await this.authenticate();

    if (typeof (callback) === 'undefined' && typeof (options) === 'function') {
        callback = options;
    }

    throw new Error('Method \'' + arguments.callee.name + '\' not Implemented');
};

FSBackblaze.prototype.mkdtemp = async function (prefix, options, callback) {
    await this.authenticate();

    if (typeof (callback) === 'undefined' && typeof (options) === 'function') {
        callback = options;
    }

    throw new Error('Method \'' + arguments.callee.name + '\' not Implemented');
};

FSBackblaze.prototype.open = async function (path, flags, mode, callback) {
    await this.authenticate();

    if (typeof (callback) === 'undefined' && typeof (mode) === 'function') {
        callback = mode;
    }

    let highestfd = 0;
    for (const fd in this.filedescriptors) {
        if (fd > highestfd) {
            highestfd = fd;
        }
    }

    this.filedescriptors[highestfd + 1] = path;
    callback(null, highestfd + 1);
};

FSBackblaze.prototype.read = async function (fd, buffer, offset, length, position, callback) {
    await this.authenticate();

    if (this.filedescriptors.hasOwnProperty(fd) === false) {
        callback('fd isn\'t a valid open file descriptor.', null);
        return;
    }

    if (offset !== null) {
        throw new Error('Setting offset to anything else than null is currently not supported!');
    }

    try {
        const res = await got({
            url: Path.posix.join(self.downloadUrl, this.filedescriptors[fd]),
            responseType: 'buffer',
            headers: {
                'Range': 'bytes=' + position + '-' + (position + length - 1),
                'Authorization': this.authorizationToken
            }
        });

        callback(null, res.body.length, res.body);
    } catch (err) {
        callback(err, null);
    }
};

FSBackblaze.prototype.readdir = async function (path, options, callback) {
    await this.authenticate();

    if (typeof (callback) === 'undefined' && typeof (options) === 'function') {
        callback = options;
    }

    const self = this;

    try {
        path = FSBackblaze.preparePath(path);
        const searchParams = {bucketId: self.bucketId, delimiter: '/'};
        if (path.length > 0) {
            searchParams['prefix'] = Path.posix.join(path, '/');
        }

        const res = await got({
            url: Path.posix.join(self.uri.href, 'b2_list_file_names'),
            method: 'GET',
            searchParams: searchParams,
            headers: {
                'Authorization': self.authorizationToken
            }
        });

        const data = JSON.parse(res.body);

        const files = [];
        for (let i = 0; i < data.files.length; i++) {
            const fileName = Path.posix.relative(Path.posix.join('/', path), Path.posix.join('/', data.files[i]['fileName'])); //decodeURIComponent(rawFileName);
            files.push(options.encoding === 'buffer' ? new Buffer(fileName) : fileName);
        }

        callback(null, files);
    } catch (err) {
        callback(err, null);
    }
};

FSBackblaze.prototype.readFile = async function (path, options, callback) {
    await this.authenticate();

    if (typeof (callback) === 'undefined' && typeof (options) === 'function') {
        callback = options;
    }

    try {
        const res = await got({
            url: Path.posix.join(self.downloadUrl, FSBackblaze.preparePath(path)),
            headers: {'Authorization': this.authorizationToken}
        });

        callback(null, res.body);
    } catch (err) {
        callback(err);
    }
};

FSBackblaze.prototype.readlink = async function (path, options, callback) {
    await this.authenticate();

    if (typeof (callback) === 'undefined' && typeof (options) === 'function') {
        callback = options;
    }

    throw new Error('Method \'' + arguments.callee.name + '\' not Implemented');
};

FSBackblaze.prototype.realpath = async function (path, options, callback) {
    await this.authenticate();

    if (typeof (callback) === 'undefined' && typeof (options) === 'function') {
        callback = options;
    }

    throw new Error('Method \'' + arguments.callee.name + '\' not Implemented');
};

FSBackblaze.prototype.rename = async function (oldPath, newPath, callback) {
    await this.authenticate();

    throw new Error('Method \'' + arguments.callee.name + '\' not Implemented');
};

FSBackblaze.prototype.rmdir = async function (path, options, callback) {
    await this.authenticate();

    if (typeof (callback) === 'undefined' && typeof (options) === 'function') {
        callback = options;
    }

    throw new Error('Method \'' + arguments.callee.name + '\' not Implemented');
};

FSBackblaze.prototype.stat = async function (path, callback) {
    await this.authenticate();

    const self = this;

    path = FSBackblaze.preparePath(path);
    if (self.statCache.hasOwnProperty(path)) {
        callback(null, self.statCache[path]);
        return;
    }

    let parentPath = Path.posix.dirname(Path.posix.join('/', path));
    parentPath = FSBackblaze.preparePath(parentPath);

    try {
        const searchParams = {bucketId: self.bucketId, delimiter: '/'};
        if (parentPath.length > 0) {
            searchParams['prefix'] = Path.posix.join(parentPath, '/');
        }

        const res = await got({
            url: Path.posix.join(self.uri.href, 'b2_list_file_names'),
            method: 'GET',
            searchParams: searchParams,
            headers: {
                'Authorization': self.authorizationToken
            }
        });

        const parentData = JSON.parse(res.body);
        let data = null;
        for (let i = 0; i < parentData.files.length; i++) {
            if (parentData.files[i].fileName === path ||
                parentData.files[i].fileName === Path.posix.join(path, '/')) {
                data = parentData.files[i];
            }
        }

        if (!data) {
            callback({code: 'ENOENT'}, null);
            return;
        }

        const statData = {
            isFile: function () {
                return data['action'] !== 'folder';
            },
            isDirectory: function () {
                return data['action'] === 'folder';
            },
            isBlockDevice: function () {
                return false;
            },
            isCharacterDevice: function () {
                return false;
            },
            isSymbolicLink: function () {
                return false;
            },
            isFIFO: function () {
                return false;
            },
            isSocket: function () {
                return false;
            },
            dev: 0,
            ino: data['action'] !== 'folder' ? parseInt(data['fileId']) : 0,
            mode: 0,
            nlink: 1,
            uid: 1,
            gid: 1,
            rdev: 0,
            size: data['action'] !== 'folder' ? parseInt(data['contentLength']) : 0,
            blksize: 4096,
            blocks: Math.ceil((data['action'] !== 'folder' ? parseInt(data['contentLength']) : 0) / 512),
            mtime: data['action'] !== 'folder' && data['fileInfo'].hasOwnProperty('src_last_modified_millis') ? new Date(parseInt(data['fileInfo']['src_last_modified_millis'])) : null,
            atime: null,
            ctime: null,
            birthtime: data['action'] !== 'folder' ? new Date(data['uploadTimestamp']) : null,
            mtimeMs: data['action'] !== 'folder' && data['fileInfo'].hasOwnProperty('src_last_modified_millis') ? parseInt(data['fileInfo']['src_last_modified_millis']) / 1000 : null,
            atimeMs: null,
            ctimeMs: null,
            birthtimeMs: data['action'] !== 'folder' ? data['uploadTimestamp'] / 1000 : null
        };

        self.statCache[path] = statData;
        callback(null, statData);
    } catch (err) {
        callback(err, null);
    }
};

FSBackblaze.prototype.symlink = async function (target, path, type, callback) {
    await this.authenticate();

    if (typeof (callback) === 'undefined' && typeof (type) === 'function') {
        callback = type;
    }

    throw new Error('Method \'' + arguments.callee.name + '\' not Implemented');
};

FSBackblaze.prototype.truncate = async function (path, len, callback) {
    throw new Error('Method \'' + arguments.callee.name + '\' not Implemented');
};

FSBackblaze.prototype.unlink = async function (path, callback) {
    throw new Error('Method \'' + arguments.callee.name + '\' not Implemented');
};

FSBackblaze.prototype.unwatchFile = function (filename, listener) {
    throw new Error('Method \'' + arguments.callee.name + '\' not Implemented');
};

FSBackblaze.prototype.utimes = function (path, atime, mtime, callback) {
    throw new Error('Method \'' + arguments.callee.name + '\' not Implemented');
};

FSBackblaze.prototype.watch = function (filename, options, listener) {
    throw new Error('Method \'' + arguments.callee.name + '\' not Implemented');
};

FSBackblaze.prototype.watchFile = function (filename, options, listener) {
    throw new Error('Method \'' + arguments.callee.name + '\' not Implemented');
};

FSBackblaze.prototype.write = async function (fd, string, position, encoding, callback) {
    if (typeof position !== 'number') {
        position = 0;
    }

    throw new Error('Method \'' + arguments.callee.name + '\' not Implemented');
};

FSBackblaze.prototype.writeFile = async function (file, data, options, callback) {
    if (typeof (callback) === 'undefined' && typeof (options) === 'function') {
        callback = options;
    }

    throw new Error('Method \'' + arguments.callee.name + '\' not Implemented');
};

module.exports = FSBackblaze;