fs-backblaze
===================
A drop-in replacement for the `fs` module, for Backblaze.
Used to treat a Backblaze bucket as a filesystem, without having to rewrite your code to switch from `fs` to `fs-backblaze`

API Key
-------------
Generate an API key for your use case. Generating keys can be done here: https://secure.backblaze.com/app_keys.htm
It's advised to create an Application Key and not use the master key.
Always generate a bucket-specific application key, a key with "all" access has not been tested.

Setup
-------------
Setting up the Backblaze filesystem only requires 1 step after which it can be used:

`const FSBackblaze = require('fs-backblaze');`
`const fs = new FSBackblaze(bucketName, applicationKeyId, applicationKey);`

Using this module
-------------
Simply use all methods the same way as you would with the regular `fs` module

Supported methods:

| Method   | Supported? |
|----------|:-------------:|
| access | no |
| appendFile | no |
| chmod | no |
| chown | no |
| close | YES |
| createReadStream | YES |
| createWriteStream | no |
| lchmod | no |
| lchown | no |
| link | no |
| lstat | no |
| mkdir | no |
| mkdtemp | no |
| open | YES |
| read | YES |
| readdir | YES |
| readFile | YES |
| readlink | no |
| realpath | no |
| rename | no |
| rmdir | no |
| stat | YES |
| symlink | no |
| truncate | no |
| unlink | no |
| unwatchFile | no |
| utimes | no |
| watch | no |
| watchFile | no |
| write | no |
| writeFile | no |

It could be that some methods with optional arguments aren't implemented the way you expect it to.
If you encounter such method, please file an issue on the Bitbucket and it will be implemented asap. PR's are very welcome too.