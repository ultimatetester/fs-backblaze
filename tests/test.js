const assert = require('chai').assert;
const BackblazeFS = require('../index');

describe('BackblazeFS', function () {
    let fs = null;

    before(function () {
        fs = new BackblazeFS('', '', '');
    });

    describe('#readdir()', function () {
        it('should list files in the root directory', function (done) {
            fs.readdir('/', function (err, files) {
                if (!!err) {
                    throw err;
                }

                assert.equal(files.length, 2, 'Incorrect amount of files returned');
                done();
            });
        });

        it('should list files in a sub-directory', function (done) {
            fs.readdir('FolderTest/', function (err, files) {
                if (!!err) {
                    throw err;
                }

                assert.equal(files.length, 2, 'Incorrect amount of files returned');
                done();
            });
        });
    });

    describe('#stat()', function () {
        it('should identify name as folder', function (done) {
            fs.stat('FolderTest/', function (err, stats) {
                if (!!err) {
                    throw err;
                }

                assert.equal(stats.isDirectory(), true, 'Incorrectly identified folder as file');
                done();
            });
        });

        it('should identify name as file', function (done) {
            fs.stat('FolderTest/testIMage2.png', function (err, stats) {
                if (!!err) {
                    throw err;
                }

                assert.equal(stats.isFile(), true, 'Incorrectly identified file as folder');
                done();
            });
        });
    });
});
